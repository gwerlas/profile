User profile
============

Get the code :

```sh
TARGET_DIR="$HOME/Workspace/"
mkdir -p "$TARGET_DIR"
git clone https://gitlab.com/gwerlas/profile.git "$TARGET_DIR"/profile
```

Install the config files and download themes and use them :

```sh
"$TARGET_DIR"/profile/init
```

Reset Gnome settings :
```sh
"$TARGET_DIR"/profile/gnome
```

Reset VSCode settings :
```sh
"$TARGET_DIR"/profile/vscode
```
