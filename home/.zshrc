setopt autocd
bindkey -e

zstyle :compinstall filename "$HOME/.zshrc"
autoload -Uz compinit
compinit

for sh in $HOME/.zsh/* ; do
	[[ -r "$sh" ]] && . "$sh"
done
unset sh
